import base64
from .get_json_data import *

def get_remote_last_commit(gl, count):
    project = gl.projects.get(get_project_id(count= count))
    branch = project.branches.get('main')
    latest_commit_sha = branch.commit['id']

    return latest_commit_sha

def get_file_content(project, file_path, commit_sha):
    file_info = project.files.get(file_path=file_path, ref=commit_sha)
    encoded_content = file_info.content
    decoded_content = base64.b64decode(encoded_content).decode('utf-8')
    return decoded_content