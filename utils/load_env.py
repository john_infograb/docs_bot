from dotenv import load_dotenv
import os

load_dotenv()

PROJECT_ID= os.getenv('PROJECT_ID')
GITLAB_PRIVATE_TOKEN=os.getenv('GITLAB_PRIVATE_TOKEN')
API_KEY=os.getenv('API_KEY')
INFOGRAB_ID=os.getenv('INFOGRAB_ID')