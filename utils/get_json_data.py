import json

def get_json() -> json:
    with open('./resource/product.json', 'r') as file:
        json_data = json.load(file)
    
    return json_data

def get_json_key(count: int) -> str:
    json_key = list(get_json().keys())[count]

    return json_key

def get_commit_sha(count : int) -> str:
    latest_commit_sha = get_json()[get_json_key(count)]["latest_commit_sha"]

    return latest_commit_sha

def get_project_id(count: int) -> json:
    project_id = get_json()[get_json_key(count)]["project_id"]

    return project_id

def get_doc_dir(count: int) -> str:
    doc_dir = get_json()[get_json_key(count)]["doc_dir"]

    return doc_dir
