import gitlab

from utils.load_env import GITLAB_PRIVATE_TOKEN
from utils.get_json_data import *
from utils.get_gitlab_data import *
from feature.operate_gitlab import *

gl = gitlab.Gitlab(url='https://gitlab.com/', private_token=GITLAB_PRIVATE_TOKEN)

def get_commit_change(count):
    my_project = gl.projects.get('53852958')
    project= gl.projects.get(get_project_id(count= 0))
    commit_sha= get_commit_sha(count= 0)
    remote_last_commit= get_remote_last_commit(gl, count= 0)
    commits = project.commits.list(ref_name=f'{commit_sha}...{remote_last_commit}', get_all=True)
    changes_info = []

    for commit in commits:
        changes = commit.diff()
        for change in changes:
            new_path = change.get('new_path', '')
            old_path = change.get('old_path', '')

            if new_path.startswith(get_doc_dir(count)) and new_path.endswith(".md"):
                if change['renamed_file']:
                    print(f"renaemd : {new_path}")
                    rename_remote_file(old_path, new_path, project, 'main', 'testestsetest1b33332', remote_last_commit)
            
                elif change['new_file']:
                    print(f"New file created: {new_path}")
                    create_remote_file(new_path, my_project, 'main', get_file_content(project, new_path, remote_last_commit))

                elif change['deleted_file']:
                    print(f"File deleted: {old_path}")
                    delete_remote_file(old_path, my_project)

                else:
                    print("file updated")
                    update_remote_file(new_path, project, commit_sha, remote_last_commit, my_project)
                    updated_content_list = update_file_diff_lines_check(project, new_path, commit_sha, remote_last_commit)
                    updated_content = '\n'.join(updated_content_list)
                    encoded_content = base64.b64encode(updated_content.encode()).decode()

                    f = my_project.files.get(file_path=new_path, ref='main')
                    f.content = encoded_content

                    f.save(branch='main', commit_message='updated', encoding='base64')
