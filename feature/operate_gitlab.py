from utils.get_gitlab_data import *

import difflib


def create_remote_file(file_path, project, branch, content):
    project.files.create({
        'file_path': file_path, 
        'branch': branch,
        'content': content,
        'author_name': "gitlab-docs-bot",
        'commit_message': ':sparkles: :: 문서 생성'
    })

def delete_remote_file(file_path, project):
    file_info = project.files.get(file_path=file_path, ref='8487b276345d5c11a80c81dfc9af4d9741211b2e')

    try: 
        if file_info:
            file_info.delete('main', 'deleted')
    except Exception as e:
        print(f"Error: {e}")

def rename_remote_file(old_path, new_file_path, project, branch, local_commit, remote_commit):
    file_info = project.files.get(file_path=old_path, ref=local_commit)
                
    if file_info:
        file_info.delete(branch, 'changed')
                        
        create_remote_file(new_file_path, project, 'main', get_file_content(project, new_file_path, remote_commit))

def update_remote_file(file_path, project, commit_sha, remote_last_commit, my_project):
    updated_content_list = update_file_diff_lines_check(project, file_path, commit_sha, remote_last_commit)
    updated_content = '\n'.join(updated_content_list)
    encoded_content = base64.b64encode(updated_content.encode()).decode()

    f = my_project.files.get(file_path=file_path, ref='main')
    f.content = encoded_content

    f.save(branch='main', commit_message='updated', encoding='base64')
    

def update_file_diff_lines_check(project, file_path, commit_sha, remote_last_commit):
    count = 0
    old_file = get_file_content(project= project, file_path= file_path, commit_sha= commit_sha)
    new_file = get_file_content(project= project, file_path= file_path, commit_sha= remote_last_commit)

    differ = difflib.Differ()
    diff = list(differ.compare(old_file.splitlines(), new_file.splitlines()))
    lines = old_file.splitlines()
    print(f"Changes in file: {file_path}")
                    
    for i, line in enumerate(diff, 1):
        if line.startswith('-'):
            print(f"Line {i} (minus): {line[2:]}")
            if 0 < i <= len(lines):
                del lines[i - 1]
                count += 1
        elif line.startswith('+'):
            target_line = i - count
            print(f"Line {i} (added): {line[2:]}")
            print(lines)
            if i == target_line:
                lines[target_line] += line[2:]
            elif i < target_line:
                continue
            else:
                lines.append(line[2:])
            count = 0

    return lines
